#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, rect: &Rectangle) -> bool {
        self.width >= rect.width && self.height >= rect.height
    }
    // "Static" function
    fn square(side_length: u32) -> Rectangle {
        Rectangle {
            width: side_length,
            height: side_length,
        }
    }
}

fn main() {
    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    let rect2 = Rectangle {
        width: 50,
        height: 50,
    };

    let rect3 = Rectangle {
        width: 22,
        height: 45,
    };

    println!("Rectangle1 info: {:?}", rect1);
    println!("Rectangle2 info: {:?}", rect2);
    println!("Rectangle3 info: {:?}", rect3);
    println!("The area of the rectangle is: {} pixels.", rect1.area());

    println!("rect1 can hold rect2: {}", rect1.can_hold(&rect2));
    println!("rect2 can hold rect3: {}", rect2.can_hold(&rect3));
    println!("rect1 can hold rect3: {}", rect1.can_hold(&rect3));

    let square = Rectangle::square(22);
    println!("Check out my square: {:?}", square);
}
